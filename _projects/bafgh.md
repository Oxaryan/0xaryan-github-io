---
title: Bafgh Automation
icon: fe fe-briefcase
is-oss: false
source-link: https://github.com/0xaryan/BafghAutomation
---
# Bafgh Automation Dashboard
Bafgh Automation is an Enterprise Automation Application for a large company in Yazd. This app includes reading weightbridge data, submit data in local database and sync with server, drag and drop report designer and more...

This project is written in both [C#](https://docs.microsoft.com/en-us/dotnet/csharp/) and [F#](https://dotnet.microsoft.com/languages/fsharp).

## Screenshots
#### Dashboard
![Bafgh Automation App - Dashboard Image](/assets/images/projects/bafgh/dashboard.jpg)

#### Report Designer
![Bafgh Automation App - Report Designer Image](/assets/images/projects/bafgh/designer.jpg)

#### Settings
![Bafgh Automation App - Settings Image](/assets/images/projects/bafgh/settings.jpg)

#### Item Codes Editor
![Bafgh Automation App - Item Codes Editor Image](/assets/images/projects/bafgh/itemcodes.jpg)