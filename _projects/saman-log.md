---
title: Saman Log
icon: fe fe-smartphone
is-oss: true
source-link: https://github.com/0xaryan/SamanLog
---
# Saman Log

Saman-Log is a Xamarin Android application for logging SMSs from Saman Bank.

Click on **Browse Source Code** to see the project in GitHub.

