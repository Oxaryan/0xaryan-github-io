---
title: Eyes Guard
icon: fe fe-eye
is-oss: true
source-link: https://github.com/0xaryan/EyesGuard
---
# Eyes Guard
Eyes Guard is a health care app for protecting your eyes while working with your desktop PC.

<a href='//www.microsoft.com/store/apps/9PHW0XFKZD7J?ocid=badge'><img width="250" src='https://assets.windowsphone.com/85864462-9c82-451e-9355-a3d5f874397a/English_get-it-from-MS_InvariantCulture_Default.png' alt='English badge'/></a>

![Eyes Guard App](/assets/images/projects/eyes-guard.JPG)