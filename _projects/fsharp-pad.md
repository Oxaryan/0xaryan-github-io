---
title: F# Pad
icon: fe fe-grid
is-oss: false
source-link: 
---

# F# Pad
F# Pad is a lightweight editor for playing with F#.

{% include cards/alert.html class="warning" body="This project is under development." %}

![F# Pad App](/assets/images/projects/fspad.png)

