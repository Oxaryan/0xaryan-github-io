---
title: Hasti Studio
icon: fe fe-monitor
is-oss: false
source-link: 
---
# Hasti Studio
Hasti Studio is a Persian GUI desktop application for teaching young devs computer science concepts, algorithm design, computer programming paradigms and many more using [Hasti Language](/projects/hasti-lang).

{% include cards/alert.html class="warning" body="This project is under development." %}
