---
title: Event Template
icon: fe fe-calendar
is-oss: true
source-link: https://github.com/0xaryan/EventTemplate
---
# Event Template

Event Template is a vanilla template I’ve created for my Events website without using any frameworks or 3rd party libraries.
See the template [here](http://aryan.software/EventTemplate).

Click on **Browse Source Code** to see the project in GitHub.

