---
title: Ocisp
icon: fe fe-book
is-oss: true
source-link: https://github.com/0xaryan/Ocisp
---
# Ocisp

An implementation of optimized crossover for independent set problem.

Article: [https://pubsonline.informs.org](https://pubsonline.informs.org/doi/10.1287/opre.45.2.226)
Implementation: [Ocisp](https://github.com/0xaryan/Ocisp)