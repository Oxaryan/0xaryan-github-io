---
title: Excel-Moodle
icon: fe fe-book-open
is-oss: true
source-link: https://github.com/0xaryan/ExcelMoodleAddin
---
# Excel Moodle Addin

A simple addin for writing Moodle questions with GIFT format, in Excel.

Click on **Browse Source Code** to see the project in GitHub.

