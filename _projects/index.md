---
title: Home
icon: fe fe-home
is-oss: false
order: -1
---

Welcome! This is the Projects section of my website. Here you can find information about tiny side project of mine to big ones.

### How to browse?
Simply use the side menu of this page for browsing.

### How to contribute?
If the project is open-source, you'll see a blue **Browse Source Code** button on top of the menu.