---
layout: talks
title: Refactoring Code - Software Talks
description: A live talk about code refactoring
image: assets/images/talks/refactoring.jpg
categories: []
tags: [Software Talks, Refactoring, Code]
comments: true
language: persian
---

<div class="talks-yt-container">
<iframe class="talks-yt-video" src="https://www.youtube.com/embed/g3A8VfJ2tOc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>