I know C# language features such as:

- All C# Keywords and Preprocessor Directives
- Delegates, Event system
- Linq
- Extension Methods
- Data Structures and Enumerables, Tuples, ...
- Attributes
- Interop and P/Invoke
- Interpolated Strings and Formatting
- Async/Await Pattern, Task and ValueTask
- OOP, Covariance and Contravariance
- Functional Features and Lambda Expressions
- C# and .NET coding conventions
- Socket Programming, Networking, Multithreading...

I have more than 3 years of professional experience in C# and .NET. I work on lots of Desktop projects and I have experience in Web Development, Game Programming, and IoT development with C#. 