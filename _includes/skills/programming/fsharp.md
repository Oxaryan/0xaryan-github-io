I'm an experienced Functional Programmer in F# and its great features such as:

- Computation Expressions like async, query, ...
- Type Providers
- Units of Measures
- Type Extensions and Abbreviations
- Data Structures like list, array, seq, ...
- Record Types
- Discriminated Unions
- Inline Optimization and Statically Resolved Types (Compile-time Generics)
- Flexible Types
- Active Patterns
- Lazy Expressions
- +All .NET features mentioned in C# section...
