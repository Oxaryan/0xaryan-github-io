 XAML or Extensible Application Markup Language is a great markup language with lots of useful features that can be used in WPF, UWP, Xamarin, Unity and a lot of other types of apps and games.
My Xaml skills includes:

- Design custom controls with custom behaviors
- Create custom MarkupExtensions
- Create custom Attached Properties
- Create responsive user interfaces