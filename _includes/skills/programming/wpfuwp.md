WPF and UWP are my UI frameworks of choice. Almost all of my desktop projects are written in these frameworks
I'm skilled in many aspects of these frameworks such as:

- Creating animations
- Creating vector graphics
- Text rendering features
- Model-View-ViewModel Pattern
- WPF and UWP Controls (DataGrid, Layouts, ... )
- Custom Behaviors and other `Microsoft.Expression.Interaction` features
- Creating Custom User Controls
- Data Binding
- Creating Styles and Templates
- Globalization and Localization (i18n)
